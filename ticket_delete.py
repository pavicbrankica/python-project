import sqlite3
dani=['ponedjeljak','utorak','srijeda','cetvrtak','petak','subota','nedjelja']
connection = sqlite3.connect('bioskop.db')
cursor = connection.cursor()
sql_command='''CREATE TABLE tickets (
dan VARCHAR PRIMARY KEY,
projekcija2D_do_17h VARCHAR,
projekcija2D_od_17h VARCHAR,
projekcija3D_do_17h VARCHAR,
projekcija3D_od_17h VARCHAR);'''
cursor.execute(sql_command)
projekcija2D_do_17h=input("Unesite cijenu ulaznice za 2D projekcije do 17h: ")+'KM'
projekcija2D_od_17h=input("Unesite cijenu ulaznice za 2D projekcije od 17h: ")+'KM'
projekcija3D_do_17h=input("Unesite cijenu ulaznice za 3D projekcije do 17h: ")+"KM"
projekcija3D_od_17h=input("Unesite cijenu ulaznice za 3D projekcije od 17h: ")+"KM"
lista=[]
for dan in dani:
    lista.append((dan,projekcija2D_do_17h,projekcija2D_od_17h,projekcija3D_do_17h,projekcija3D_od_17h))

connection.executemany("""
                      INSERT INTO
                      tickets(dan,projekcija2D_do_17h,projekcija2D_od_17h,projekcija3D_do_17h,projekcija3D_od_17h)
                      VALUES (?,?,?,?,?)""",lista)
print('Sadrzaj tabele: ')
sql="""
SELECT * FROM tickets;"""
cursor.execute(sql)
result=cursor.fetchall()
for i in result:
    print(i)
cursor.execute("""DELETE FROM tickets WHERE dan="nedjelja";""")
print("Nedjelja je neradna: ")
sql="""
SELECT * FROM tickets;"""
cursor.execute(sql)
result=cursor.fetchall()
for i in result:
    print(i)
connection.commit()
connection.close()