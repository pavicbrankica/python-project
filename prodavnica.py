import sqlite3
porcije=['mala', 'srednja', 'velika']
connection = sqlite3.connect('bioskop.db')
cursor = connection.cursor()
sql_command='''CREATE TABLE snacks_drinks (
naziv VARCHAR PRIMARY KEY,
cijena DECIMAL,
kolicina INTEGER);'''
cursor.execute(sql_command)
lista=[]
for porcija in porcije:
    cijena=float(input("Cijena kokica, "+porcija+" porcija: "))
    kolicina='neograniceno'
    lista.append(("Kokice,"+porcija,cijena,kolicina))
for porcija in porcije:
    cijena=float(input("Cijena Coca-Cole, "+porcija+" porcija: "))
    kolicina='neograniceno'
    lista.append(("Coca-Cola,"+porcija,cijena,kolicina))
for porcija in porcije:
    cijena=float(input("Cijena nachos-a, "+porcija+" porcija: "))
    kolicina='neograniceno'
    lista.append(("Nachos,"+porcija,cijena,kolicina))
n=int(input("Koliko vrsta sokova i vode imate: "))
while n>0:
    naziv=input("Naziv soka/vode: ")
    cijena=float(input("Cijena soka/vode: "))
    kolicina=input("Kolicina soka/vode: ")
    lista.append((naziv, cijena, kolicina) )
    n-=1
m=int(input("Koliko vrsta slatkisa imate: "))
while m>0:
    naziv=input("Naziv slatkisa: ")
    cijena=float(input("Cijena slatkisa: "))
    kolicina=input("Kolicina slatkisa: ")
    lista.append((naziv, cijena, kolicina))
    m-=1
connection.executemany("""
                    INSERT INTO 
                    snacks_drinks (naziv, cijena, kolicina)
                    VALUES (?,?,?)""", lista)

sql="""
SELECT * FROM snacks_drinks;"""
cursor.execute(sql)
result=cursor.fetchall()
for i in result:
    print(i)

connection.commit()
connection.close()