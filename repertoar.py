import sqlite3
from sqlite3 import Error


class Film:
    def __init__(self, naziv, zanr, jezik, prevod, trajanje):
        self.naziv = naziv
        self.zanr = zanr
        self.jezik = jezik
        self.prevod = prevod
        self.trajanje = trajanje

    def __str__(self):
        return "Naziv filma: " + self.naziv + ", zanr: " + self.zanr + ", jezik:"\
                            + self.jezik + ", prevod " + self.prevod + ", trajanje: " + self.trajanje + 'min'

def create_connection(db_file):
    conn = None
    try:
        conn = sqlite3.connect(db_file)
        return conn
    except Error as e:
        print(e)

    return conn


def create_table(conn, create_table_sql):
    try:
        c = conn.cursor()
        c.execute(create_table_sql)
    except Error as e:
        print(e)

def create_film(conn, film):

    sql = ''' INSERT INTO films(naziv,zanr,jezik,prevod,trajanje)
              VALUES(?,?,?,?,?) '''
    cur = conn.cursor()
    params = (film.naziv, film.zanr, film.jezik, film.prevod, film.trajanje)
    cur.execute(sql, params)
    conn.commit()
    return cur.lastrowid


if __name__ == '__main__':
    sql_create_film_table = """ CREATE TABLE IF NOT EXISTS films (
                                           naziv text PRIMARY KEY,
                                           zanr text NOT NULL,
                                           jezik text NOT NULL,
                                           prevod text NOT NULL,
                                           trajanje integer NOT NULL
                                       ); """

    conn = create_connection("bioskop.db")

    if conn is not None:
        create_table(conn, sql_create_film_table)
        br_filmova = int(input("Unesite broj filmova koji su trenutno na repertoaru: "))
        while br_filmova > 0:
            naziv = input("Unesite naziv filma: ")
            zanr = input("Unesite zanr filma: ")
            jezik = input("Unesite jezik filma: ")
            prevod = input("Da li film ima prevod? DA/NE ")
            trajanje = int(input("Koliko minuta traje film? "))
            f = Film(naziv, zanr, jezik, prevod, trajanje)
            project_id = create_film(conn, f)
            br_filmova -= 1
    else:
        print("Error! cannot create the database connection.")

cursor = conn.cursor()
sql = """
SELECT * FROM films;"""
cursor.execute(sql)
result = cursor.fetchall()
for i in result:
    print(i)
conn.commit()
conn.close()


