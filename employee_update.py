import sqlite3

connection = sqlite3.connect('bioskop.db')
cursor = connection.cursor()
sql_command='''CREATE TABLE employees (
jmbg VARCHAR PRIMARY KEY,
ime VARCHAR(20),
prezime VARCHAR(20),
pol VARCHAR,
bracni_status VARCHAR,
pozicija VARCHAR(20));'''
cursor.execute(sql_command)
lista=[]
n=int(input("Unesite broj zaposlenih u bioskopu: "))
while n>0:
    a=input("Unesite jmbg zaposlenog: ")
    b=input("Unesite ime zaposlenog: ")
    c=input("Unesite prezime zaposlenog: ")
    d=input("Unesite pol zaposlenog (M/Z): ")
    e=input ("Unesite bracni status zaposlenog (single/u braku): ")
    f=input("Unesite poziciju zaposlenog: ")
    if d=='Z' and e=='u braku':
        h=input('Unesite djevojacko prezime: ')
        update_prezime=h+'-'+c
        c=update_prezime
    lista.append((a,b,c,d,e,f))
    n-=1
connection.executemany("""
                    INSERT INTO 
                    employees(jmbg,ime,prezime,pol,bracni_status,pozicija)
                    VALUES (?,?,?,?,?,?)""",lista)
print('Sadrzaj tabele: ')
sql="""
SELECT * FROM employees;"""
cursor.execute(sql)
result=cursor.fetchall()
for i in result:
    print(i)

connection.commit()
connection.close()