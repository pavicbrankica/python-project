import sqlite3

connection = sqlite3.connect('bioskop.db')
cursor = connection.cursor()
sql_command='''CREATE TABLE employees (
jmbg VARCHAR PRIMARY KEY,
ime VARCHAR(20),
prezime VARCHAR(20),
pol VARCHAR,
bracni_status VARCHAR,
pozicija VARCHAR(20),
godiste INTEGER);'''
cursor.execute(sql_command)
lista=[]
n=int(input("Unesite broj zaposlenih u bioskopu: "))
while n>0:
    a=input("Unesite jmbg zaposlenog: ")
    b=input("Unesite ime zaposlenog: ")
    c=input("Unesite prezime zaposlenog: ")
    d=input("Unesite pol zaposlenog (M/Z): ")
    e=input ("Unesite bracni status zaposlenog (single/u braku): ")
    f=input("Unesite poziciju zaposlenog: ")
    g=int(a[5]+a[6])
    lista.append((a,b,c,d,e,f,g))
    n-=1
connection.executemany("""
                    INSERT INTO 
                    employees(jmbg,ime,prezime,pol,bracni_status,pozicija,godiste)
                    VALUES (?,?,?,?,?,?,?)""",lista)
print('Sadrzaj tabele: ')
sql="""
SELECT * FROM employees;"""
cursor.execute(sql)
result=cursor.fetchall()
for i in result:
    print(i)
cursor.execute('''DELETE FROM employees WHERE godiste<57;''')
print("Brisanje zaposlenih preko 65 godina: ")
sql="""
SELECT * FROM employees;"""
cursor.execute(sql)
result=cursor.fetchall()
for i in result:
    print(i)
connection.commit()
connection.close()